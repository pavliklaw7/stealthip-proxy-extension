document.addEventListener('DOMContentLoaded', () => {

  injectRTCScript();

  function injectRTCScript() {
    var script = document.getElementById("webrtc-control");
    var head = document.documentElement || document.head || document.querySelector("head");

    if (!script) {
        script = document.createElement('script');
        script.type = "text/javascript";
        script.setAttribute("id", "webrtc-control");
        if (head) head.appendChild(script);
    }
  };
});
