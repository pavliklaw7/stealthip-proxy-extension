
/* Global Variables */
let popupUrl = '';
let fatal;

chrome.runtime.onInstalled.addListener(async () => {
  setWebRTC();
  // chrome.extension.sendMessage(new ExtensionMessage(config.keys.fetchWhiteList, {}));
  await storageSet({key: config.keys.savedProxy, value: []});
});

chrome.proxy.onProxyError.addListener((error) => {
  
  console.log('onProxError', error.error);

  if (error.fatal && popupUrl) {
    killSwitch(fatal)
  }
});

setKillSwitchStatus();

async function setKillSwitchStatus() {
  await storageSet({key: config.keys.isKillSwitchActive, value: false});
}

chrome.runtime.onMessage.addListener(
  async function(message){
    switch(message.context){
      case config.keys.setProxy:
        setProxy(message.data.formData);
        console.log(`Proxy seted: ${JSON.stringify(message.data.formData)}`);
        break;
      case config.keys.clearProxy:
        clearProxy();
        if (chrome.webRequest.onBeforeRequest.hasListener(blockRequest)) {
          console.log('listener removed');
          chrome.webRequest.onBeforeRequest.removeListener(blockRequest);
        }
        console.log('Proxy cleared');
        break;
      case config.keys.proxyConnectionCheck:
        proxyConnectionCheck();
        break;
      case config.keys.killSwitch:
        console.log('popup url seted');
        popupUrl = message.data.popupUrl;
        break;
    };
  }
);

function proxyConnectionCheck() {
  chrome.proxy.settings.get({}, function(configuration) {
    chrome.extension.sendMessage(new ExtensionMessage(config.keys.proxyConnectionCheck,
      {data: JSON.stringify(configuration.value.mode)}));
  });
}

function setProxy(proxy) {
  const proxyConfig = {
    mode: "fixed_servers",
    rules: {
      singleProxy: {
        scheme: proxy.scheme,
        host: proxy.host,
        port: parseInt(proxy.port)
      },
    }
  };

  console.log(proxyConfig);

  // chrome.proxy.settings.set(
  //   {value: proxyConfig, scope: 'regular'}, async () => {
  //     await storageSet({key: 'currentProxyMode', value: proxyConfig.mode});
  //   }
  // );

  try {
    chrome.proxy.settings.set(
      {value: proxyConfig, scope: 'regular'}, async () => {
        await storageSet({key: 'currentProxyMode', value: proxyConfig.mode});
        
        const currentSevedProxy = await storageGet(config.keys.savedProxy);
        await storageSet({key: config.keys.savedProxy, value: [...currentSevedProxy, proxyConfig.rules.singleProxy]});
      }
    );
  }
  catch {
    console.log('set Proxy error catch')
    chrome.extension.sendMessage(new ExtensionMessage(config.keys.displayError));
  }
};

function setWebRTC() {
  const IPHandlingPolicy = {"value": 'disable_non_proxied_udp'};

  chrome.privacy.network.webRTCIPHandlingPolicy.set(IPHandlingPolicy, function() {
    chrome.privacy.network.webRTCIPHandlingPolicy.get({}, function(e) {
        // console.error("IPHandlingPolicy: ", e.value);
    });
  });
}


function clearProxy() {
  chrome.proxy.settings.clear({}, async () => {});
};

function blockRequest(details) {
  if (details.url.includes(popupUrl)) {
    console.log('cancel: false')
    console.log(popupUrl)
    console.log(details.url)
    return {cancel: false};
  }
  console.log('cancel: true')
  console.log(details.url)
  return {cancel: true};
}

function killSwitch(status) {
  if (chrome.webRequest.onBeforeRequest.hasListener(blockRequest)) {
    console.log('listener removed');
    chrome.webRequest.onBeforeRequest.removeListener(blockRequest);
  }

  if (status) {
    try {
      console.log('listener added');
      chrome.webRequest.onBeforeRequest.addListener(blockRequest, {
        urls: ["<all_urls>"]
      }, ['blocking']);
    } catch (e) {
      console.error(e);
    }
  } else {
    console.log('return');
    return;
  }
}
