var config = {
  keys: {
    setProxy: 'setProxy',
    clearProxy: 'clearProxy',
    getDefaultProxy: 'getDefaultProxy',
    getIp: 'getIp',
    popupUrl: 'popupUrl',
    proxyConnectionCheck: 'proxyConnectionCheck',
    system: 'system',
    proxyError: 'proxyError',
    displayError: 'displayError',
    killSwitch: 'killSwitch',
    isKillSwitchActive: 'isKillSwitchActive',
    setKillSwitchStatus: 'setKillSwitchStatus',
    setWebRTC: 'setWebRTC',
    saveProxyConfig: 'saveProxyConfig',
    savedProxy: 'savedProxy'
  },
  values: {
    greenColor: '#81DB9A',
    redColor: '#F2645E',
    connectText: 'CONNECT',
    disconnectText: 'DISCONNECT',
    errorMessage: 'Please, check your input data.',
    killSwitchOnText: 'KillSwitch is now enabled',
    killSwitchOffText: 'KillSwitch is now disabled',
    whitelist: [
    ],
  },
};
