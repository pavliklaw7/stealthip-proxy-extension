init();


async function init(){

  /* Global Variables */

  const {
    isKillSwitchActive,
    killSwitch
  } = config.keys;

  const scheme = document.querySelector('#scheme');
  const host = document.querySelector('#host');
  const port = document.querySelector('#port');
  var selectConfig = document.querySelector('#selectConfig');
  selectConfig.required = false;

  scheme.oninput = () => {
    selectConfig.value = "Select the Host"
  }
  host.oninput = () => {
    selectConfig.value = "Select the Host"
  }
  port.onchange = () => {
    selectConfig.value = "Select the Host"
  }

  const {
    redColor,
    greenColor,
    connectText,
    disconnectText,
    killSwitchOnText,
    killSwitchOffText
  } = config.values;

  
  let currentProxyMode = await requestBackground(new ExtensionMessage(config.keys.proxyConnectionCheck));

  chrome.extension.onMessage.addListener((message) => {
    switch (message.context) {
      case config.keys.displayError:
        displayError();
        break;
      case config.keys.saveProxyConfig:
        config.values.savedProxy.push(message.data); // set to storate 
    }
  })

  const form = document.querySelector('#proxy-data');
  const button = document.querySelector('.popup__action');
  const inputs = Array.from(form.children);

  for(let input of inputs){
    input.oninput = async (e) => {
      await storageSet({key: "input_" + e.target.id, value: e.target.value})
    }
  }

  for(let input of inputs){
    if(input.querySelector("input")?.type == "text"){
      input.querySelector("input").value = (await storageGet("input_" + input.querySelector("input").id)) ? (await storageGet("input_" + input.querySelector("input").id)) : ""
    }
  }
  
  autoFill();
  switchBtnStyle();
  killSwitchFunction();

  async function autoFill() {

    const currentProxyConfig = await storageGet(config.keys.savedProxy);


    currentProxyConfig.forEach(config => {
      const arrayOfConfigs = Array.from(selectConfig.children).map(option => option.value);

      if (arrayOfConfigs.includes(config.host)) {
        return;
      } else {
        selectConfig.insertAdjacentHTML(
          'afterbegin',
        `<option class="select__option" data-value='${JSON.stringify(config)}' value="${config.host}">${config.host}:${config.port}</option>`
        )
      }
    })

    selectConfig.addEventListener('change', (e) => {
      scheme.value = JSON.parse(e.target.options[e.target.selectedIndex].getAttribute('data-value')).scheme;
      host.value = JSON.parse(e.target.options[e.target.selectedIndex].getAttribute('data-value')).host;
      port.value = +JSON.parse(e.target.options[e.target.selectedIndex].getAttribute('data-value')).port;
    })
  }


  form.addEventListener('submit', async (event) => {
    event.preventDefault()

    const formData = {
      scheme: scheme.value.trim(),
      host: host.value.trim(),
      port: port.value,
    }

    console.log(formData);

    if (button.innerText === disconnectText) {
      chrome.runtime.sendMessage(new ExtensionMessage(config.keys.clearProxy, {} ));
      currentProxyMode = await requestBackground(new ExtensionMessage(config.keys.proxyConnectionCheck))
      for(let input of inputs){
        if(input.querySelector("input")?.type == "text"){
          input.querySelector("input").value = ""
          await storageSet({key: "input_" + input.querySelector("input").id, value: null})
        }
      }
    } else {
      let whitelist = await fetchWhiteList();
      if(!whitelist.includes(formData.host + ":" + formData.port)){
        simpleNotify.notify("Wrong host or port", "danger", 6000);
        return;
      }
      chrome.runtime.sendMessage(new ExtensionMessage(config.keys.setProxy, {formData} ));
      currentProxyMode = await requestBackground(new ExtensionMessage(config.keys.proxyConnectionCheck))
      console.log(config.keys.setProxy, formData);
      console.log('currentProxyMode', currentProxyMode);
      selectConfig.insertAdjacentHTML(
        'afterbegin',
        `<option class="select__option" data-value='${JSON.stringify(formData)}' value="${formData.host}">${formData.host}:${formData.port}</option>`
      )
    }
  
    switchBtnStyle();
    inputs.forEach(input => input.value = '');
  });


  function switchBtnStyle() {
    if (JSON.parse(currentProxyMode.data) !== config.keys.system) {
      button.style.color = greenColor;
      button.innerText = disconnectText;
      inputs.forEach(el => el.disabled = true)
    } else {
      button.style.color = redColor;
      button.innerText = connectText;
      inputs.forEach(el =>  el.id !== 'selectConfig' ? el.required = true : el.required = false)
      inputs.forEach(el => el.disabled = false)
    }
  }

  function displayError() {
    console.log('Hello from dislayError');
    simpleNotify.notify(config.values.errorMessage, "danger", 3500);
  }

  async function killSwitchFunction() {
    const killSwitchBtn = document.querySelector('#killSwitch');
    const switchStatus = document.querySelector('.switch__status');

    let currentKillSwitchStatus = await storageGet(isKillSwitchActive);

    if (currentKillSwitchStatus) {
      killSwitchBtn.checked = true;
      switchStatus.innerText = killSwitchOnText;  
    }

    killSwitchBtn.addEventListener('change', async () => {

      if (killSwitchBtn.checked) {
        await storageSet({key: isKillSwitchActive, value: true});
        switchStatus.innerText = killSwitchOnText;
        chrome.runtime.sendMessage(new ExtensionMessage(killSwitch, {status: true, popupUrl: location.origin}));
      } else {
        switchStatus.innerText = killSwitchOffText;
        await storageSet({key: isKillSwitchActive, value: false});
        chrome.runtime.sendMessage(new ExtensionMessage(killSwitch, {status: false, popupUrl: location.origin}));
      }
    })
  };

  async function fetchWhiteList() {
    return new Promise(async (resolve) => {
      const whiteList = (await fetch(`http://5.79.109.91/wl/whitelist?a=${Math.random()}`).then(res => res.text())).replace(/[\s\r\n]+/gi, "");
      let arr = []
  
      whiteList.split(',').forEach(whiteHost => arr.push(whiteHost));
  
      resolve(arr);
    })
  }

}
