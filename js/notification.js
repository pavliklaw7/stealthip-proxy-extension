function notification({title, message, icon="icon"}) {
  const notifyId = Date.now().toString();

  const options = {
    type: 'basic',
    iconUrl: `images/${icon}.png`,
    title: title,
    message: message,
  };

  return new Promise((resolve) => {
    chrome.notifications.create(
      notifyId, options, function(){resolve()}
    );
  });
}
